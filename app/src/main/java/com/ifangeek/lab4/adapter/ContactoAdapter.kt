package com.ifangeek.lab4.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ifangeek.lab4.R
import com.ifangeek.lab4.model.Contacto

class ContactoAdapter(
    val listContactos: MutableList<Contacto>,
    val itemCallbackCallContacto:(nroContacto : Int) -> Unit
) : RecyclerView.Adapter<ContactoAdapter.ContactoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_contacto, parent, false)
        return ContactoViewHolder(view)
    }

    override fun getItemCount(): Int = listContactos.size

    override fun onBindViewHolder(holder: ContactoViewHolder, position: Int) {
        val contacto = listContactos[position]

        holder.tvInicial.text = contacto.nombres[0].toString()
        holder.tvNombre.text = contacto.nombres
        holder.tvCargo.text = contacto.cargo
        holder.tvCorreo.text = contacto.correo

        holder.ivCallContact.setOnClickListener {
            itemCallbackCallContacto(contacto.numero)
        }

    }

    class ContactoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvInicial: TextView = itemView.findViewById(R.id.tvInicial)
        var tvNombre: TextView = itemView.findViewById(R.id.tvContactoNombre)
        var tvCargo: TextView = itemView.findViewById(R.id.tvContactoCargo)
        var tvCorreo: TextView = itemView.findViewById(R.id.tvContactoCorreo)
        var ivCallContact: ImageView = itemView.findViewById(R.id.ivCallContact)
    }
}
