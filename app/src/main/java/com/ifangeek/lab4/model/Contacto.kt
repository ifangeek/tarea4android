package com.ifangeek.lab4.model

import java.io.Serializable

data class Contacto(
    val nombres: String,
    val cargo: String,
    val correo: String,
    val numero: Int
) : Serializable
