package com.ifangeek.lab4

import android.Manifest
import android.Manifest.permission.CALL_PHONE
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.ifangeek.lab4.adapter.ContactoAdapter
import com.ifangeek.lab4.model.Contacto
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var contacto = mutableListOf<Contacto>()
    lateinit var adapterContacto: ContactoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadData()
        configAdapter()
    }

    private fun configAdapter() {

        adapterContacto = ContactoAdapter(contacto) { numeroContacto ->
            val intent = Intent(Intent.ACTION_CALL).apply {
                data = Uri.parse("tel:$numeroContacto")
            }

            if (ContextCompat.checkSelfPermission(applicationContext, CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                startActivity(intent)
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(arrayOf(CALL_PHONE), 1)
                }
            }
        }

        rvContacto.apply {
            adapter = adapterContacto
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    private fun loadData() {
        contacto.add(
            Contacto(
                "Diego Pacheco",
                "Desarrollador Android",
                "dpachecorosas96@gmail.com",
                948429548
            )
        )

        contacto.add(
            Contacto(
                "Alessandra Pacheco ",
                "Diseñadora digital",
                "sofiapacheco@gmail.com",
                912784657
            )
        )
        contacto.add(
            Contacto(
                "Valeria Pacheco",
                "Diseñadora digital",
                "sofiapacheco@gmail.com",
                943743778
            )
        )
        contacto.add(
            Contacto(
                "Elena Rosas",
                "Directora ejecutiva",
                "erosas@gmail.com",
                969283723
            )
        )
        contacto.add(
            Contacto(
                "Jose",
                "Tecnico Informatica",
                "jpachecoj@gmail.com",
                989685858
            )
        )
        contacto.add(
            Contacto(
                "Miriam",
                "Asistente adminsitrativo",
                "miriam@gmail.com",
                945868684
            )
        )
        contacto.add(
            Contacto(
                "Geraldine Bustamante",
                "Soporte TI",
                "gbustamante@gmail.com",
                921219455
            )
        )
    }
}